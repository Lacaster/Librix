extends Node2D

var mascote_preload
var mascote
var num
var x
var y
var r
var posx
var posy
var num_pos
var matrix



func _ready():
	mascote_preload = weakref(load("res://Scenes/Game/Mascote.tscn"))
	matrix = get_parent().matrix


func gera_mascote_1(n):

	mascote = mascote_preload.get_ref().instance();
	mascote.init(n)
	add_child(mascote)
	num_pos = 1
	r = 1
	set_pos(0,0)
	
func gera_mascote_2(anterior,n):
	mascote = mascote_preload.get_ref().instance();
	mascote.init(n)
	add_child(mascote)
	num_pos = 2
	r = 1
	set_pos(1,0)

	while(anterior == mascote.tipo_mascote):
		remove_child(mascote)
		mascote.free()
		mascote = mascote_preload.get_ref().instance();
		mascote.init(n)
		add_child(mascote)



func update_mascote(tipo_mascote,n):

	mascote.update_mascote(tipo_mascote, n)




func set_pos(x, y):
	self.x = x
	self.y = y

# move em coordeandas
func move(gx, gy):
	
	posx = get_position().x + (gx * 105)
	posy = get_position().y + (gy * 105)
	
	set_position(Vector2(posx,posy))
	
func move_vertical_to(gy):

	posy = get_position().y + (gy - y) * 105
	
	set_position(Vector2(get_position().x, posy))



func rotate():
	var lr = self.r
	
	
	if(lr == 1):
		# => 2
		move(-1,1)

	if(lr == 2):
		# => 3
		move(-1,-1)

	if(lr == 3):
		# => 4
		move(1,-1)

	if(lr == 4):	
		# => 1
		move(1,1)

	
	lr += 1
	
	if(lr == 5):
		lr = 1
	
	self.r = lr

func _exit_tree():
	queue_free()


func free_slot():
	mascote.free()
