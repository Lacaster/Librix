extends Node2D

var exit_button
var try_again_button
var optionChange

func _ready():
	try_again_button = get_node("Control/TryButtonAgain")
	try_again_button.grab_focus()

	optionChange = get_node("OptionChange")
	
	var best_points = int(self.read_best_points())
	var actual_points = Transition.actual_points
	
	if(actual_points == null):
		actual_points = 0
	
	if(actual_points > best_points):
		best_points = actual_points
		self.write_best_points(actual_points)
	
	get_node("PontosAtual").set_text(str(actual_points))
	get_node("PontosBest").set_text(str(best_points))

func _input(event):
	if(Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right")):
		optionChange.play()

func _on_ExitButton_pressed():
	Transition.free_child_level_node()
	Transition.fade_black_to("res://Scenes/Intro/StageScreen.tscn", null)
	Transition.stop_music()
	Transition.play_music("MusicIntro")
	Transition.clear_above()
	queue_free()



func _on_TryButtonAgain_pressed():
	Transition.free_child_level_node()
	if(Transition.actual_stage == "gnome"):
		Transition.fade_black_to("res://Scenes/Game/Level.tscn", "gnome")
		Transition.stop_music()
		Transition.play_music("MusicGnome")
	
	if(Transition.actual_stage == "kde"):
		Transition.fade_black_to("res://Scenes/Game/Level.tscn", "kde")
		Transition.stop_music()
		Transition.play_music("MusicKDE")
	Transition.clear_above()
	queue_free()

func read_best_points():
	var file = File.new()
	file.open("user://" + Transition.actual_stage + ".txt", file.READ)
	var text = file.get_as_text()
	var points = text.split("\n")
	file.close()
	return points[0]

func write_best_points(p):
	var file = File.new()
	file.open("user://" + Transition.actual_stage + ".txt", file.WRITE)
	file.store_string(str(p))
	file.close()
