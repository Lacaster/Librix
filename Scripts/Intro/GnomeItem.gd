extends Button


var stageChange

func _ready():
	stageChange = get_node("../../StageChange")


func _on_Gnome_pressed():
	get_node("AudioStageBegin").play()
	Transition.fade_black_to("res://Scenes/Game/Level.tscn", "gnome")
	Transition.stop_music()
	Transition.play_music("MusicGnome")


func _on_TextureButton_pressed():
	get_node("AudioStageBegin").play()
	Transition.fade_black_to("res://Scenes/Game/Level.tscn", "gnome")
	Transition.stop_music()
	Transition.play_music("MusicGnome")


func _on_TextureButton_mouse_entered():
	stageChange.play()
	grab_focus()




