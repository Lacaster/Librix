extends AnimationPlayer

signal transition_completed


func animation_finished():
	Transition.clear_above()