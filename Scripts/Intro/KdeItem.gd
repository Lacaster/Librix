extends Button

var stageChange

func _ready():
	stageChange = get_node("../../StageChange")


func _on_TextureButton_mouse_entered():
	stageChange.play()
	grab_focus()


func _on_TextureButton_mouse_exited():
	get_node("ColorRect").color = Color(1,1,1,1)


func _on_TextureButton_pressed():
	get_node("AudioStageBegin").play()
	Transition.fade_black_to("res://Scenes/Game/Level.tscn", "kde")
	Transition.stop_music()
	Transition.play_music("MusicKDE")

func _on_Kde_pressed():
	get_node("AudioStageBegin").play()
	Transition.fade_black_to("res://Scenes/Game/Level.tscn", "kde")
	Transition.stop_music()
	Transition.play_music("MusicKDE")
