extends Node2D

var slot1
var slot2

func _ready():
	slot1 = get_node("Slot1")
	slot2 = get_node("Slot2")

func gera_2_mascotes(n):
	slot1.gera_mascote_1(n)
	slot2.gera_mascote_2(slot1.mascote.tipo_mascote,n)
	
func remove_mascotes():
	slot1.remove_mascote()
	slot2.remove_mascote()

func _exit_tree():

	slot1.free()
	slot2.free()
	queue_free()
