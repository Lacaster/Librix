extends Node

var levelsControl
var stageChange

func _ready():
	levelsControl = get_node("Levels")
	levelsControl.get_child(0).grab_focus()
	stageChange = get_node("StageChange")
	Transition.actual_node = get_tree().get_current_scene()


func _input(event):
	if(Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right")):
		stageChange.play()
	if(Input.is_action_pressed("ui_cancel")):
		get_node("AudioBack").play()
		#Transition.free_actual_node()
		Transition.fade_black_to("res://Scenes/Intro/IntroScreen.tscn", null)


	if(Input.is_action_pressed("ui_up")):
		stageChange.play()
		get_node("Control/BackButton").grab_focus()
	if(Input.is_action_pressed("ui_down")):
		stageChange.play()
		get_node("Levels/Gnome").grab_focus()

func _on_BackButton_pressed():
	get_node("AudioBack").play()
	Transition.fade_black_to("res://Scenes/Intro/IntroScreen.tscn", null)



func _on_Label_gui_input(ev):
	pass
