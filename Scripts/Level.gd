extends Node

var pecaManager
var pontuacaoAtualLevel
var pontuacaoMaximaLevel


var levelBackground

var progressBar
var progressBarEfeitos

var levelName
var levelNumber
var levelNumberStatusBar

var pecaPreview

var labelPontos

var velocidade
var qtdTipoPecas

func _ready():
	
	velocidade = 30
	pontuacaoAtualLevel = 0
	pontuacaoMaximaLevel = 100
	pecaManager = get_node("Pecas")
	levelBackground = get_node("Background")
	progressBar = get_node("NivelBar/ProgressBar")
	progressBarEfeitos = get_node("NivelBar/Efeitos")
	levelNumberStatusBar = get_node("StatusBar/LevelNumero")
	labelPontos = get_node("StatusBar/LabelPontosNumero")
	pecaPreview = get_node("StatusBar/PecaPreview")
	levelNumber = 1
	qtdTipoPecas = 4
	Transition.actual_node = get_tree().get_current_scene()
	Transition.level_node = get_tree().get_current_scene()

	
	
	if(Transition.actual_stage == "kde"):
		levelName = "KDE"
		levelBackground.texture = load("res://Assets/Backgrounds/kde.png")
	elif(Transition.actual_stage == "gnome"):
		levelName = "GNOME"
		levelBackground.texture = load("res://Assets/Backgrounds/gnome.png")
	
	
	update_progress_bar()
	progressBarEfeitos.play()
	pecaManager.velocidade = velocidade
	pecaManager.qtdTipoPecas = qtdTipoPecas
	pecaPreview.gera_2_mascotes(qtdTipoPecas)
	pecaManager.gerar_peca(velocidade)
	update_preview_slot(qtdTipoPecas)
	get_node("StatusBar/LevelName").text = levelName
	update_status_bar()



func _input(event):
	if(Input.is_action_pressed("ui_cancel")):
		Transition.put_above("res://Scenes/Game/Pause.tscn")



func _on_Pecas_add_points(p):
	pontuacaoAtualLevel += p
	atualiza_qtd_tipo_pecas()
	update_progress_bar()
	update_status_bar()
	update_points_bar()
	update_transition_points()



func update_progress_bar():
	var local_value = (100 / pontuacaoMaximaLevel) * pontuacaoAtualLevel 
	progressBar.value =  local_value % 100
	
	if(local_value > pontuacaoMaximaLevel * levelNumber - 1):
		#executa animação efeito
		progressBarEfeitos.stop()
		progressBarEfeitos.set_animation("Next-fase")
		progressBarEfeitos.play()
		get_node("NivelBar/NextFase").play()
		levelNumber += 1
		if(levelName == "KDE"):
			velocidade = 30 + 15*levelNumber
		elif(levelName == "GNOME"):
			velocidade = 30 + 25*levelNumber
		pecaManager.velocidade = velocidade
		pecaManager.qtdTipoPecas = qtdTipoPecas

func update_status_bar():
	levelNumberStatusBar.text = str(levelNumber)

func update_points_bar():
	labelPontos.text = str(pontuacaoAtualLevel)

func update_preview_slot(n):
	pecaPreview.remove_mascotes()
	pecaPreview.gera_2_mascotes(n)

func update_transition_points():
	Transition.actual_points = pontuacaoAtualLevel

func get_mascote_1():
	return pecaPreview.slot1.mascote

func get_mascote_2():
	return pecaPreview.slot2.mascote

func atualiza_qtd_tipo_pecas():
	if(levelNumber == 1):
		qtdTipoPecas = 4
	if(levelNumber == 2):
		qtdTipoPecas = 5
	if(levelNumber == 3):
		qtdTipoPecas = 6
	if(levelNumber == 4):
		qtdTipoPecas = 7
	if(levelNumber >= 5):
		qtdTipoPecas = 8
	if(levelName == "KDE" and levelNumber >= 5):
		qtdTipoPecas = 9


